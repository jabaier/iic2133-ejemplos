#include <stdio.h>
#include <assert.h>
#include <limits.h>
#include "config.h"
#include "utils.h"


void selection(int A[], int n) {
  int i=1;
  for (i=0; i<n; i++) {
    int min_index=i;
    int j;
    for (j=i+1; j<n; j++) {
      if (A[min_index]>A[j]) {
	min_index=j;
      }
    }
    array_swap(A,min_index,i);
  }
}

int A[NUMBER];

int main() {
  int k=0;

  assert(NUMBER<INT_MAX);

  k=read_array(A);

  selection(A,k);

  print_array(A,k);

  return 0;
}
