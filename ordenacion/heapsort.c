#include <stdio.h>
#include <assert.h>
#include <limits.h>
#include "config.h"
#include "utils.h"
#include "intheap.h"

int A[NUMBER];

int main() {
  int k=0;
  
  assert(NUMBER<INT_MAX);
  
  k=read_array_one(A);
  
  heap_sort(A,k);

  print_array_one(A,k);

  return 0;

}
