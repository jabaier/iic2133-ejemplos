#define KEY(x) TREETYPE_KEY(x->element)

struct node {
  TREETYPE element;
  struct node *left;
  struct node *right;
  struct node *parent;
};

struct node* insert(struct node *node, TREETYPE element);
int maxDepth(struct node* node);
int minDepth(struct node* node);
