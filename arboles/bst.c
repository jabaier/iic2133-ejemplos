#include <stdlib.h>

struct node* insert(struct node *node, TREETYPE element) {
  struct node *newnode = (struct node *) malloc(sizeof(struct node));
  newnode->left=newnode->right=newnode->parent=NULL;
  newnode->element=element;
  struct node *root=node;
  struct node *parent;

  if (node==NULL) return newnode;
  
  while (node!=NULL) {
    parent=node;
    if (KEY(newnode)<=KEY(node))
      node=node->left;
    else 
      node=node->right;
  }
  if (KEY(newnode)<=KEY(parent))
    parent->left=newnode;
  else 
    parent->right=newnode;
  newnode->parent=parent;
  return root;
}



int maxDepth(struct node* node) {
  if (node==NULL) {
    return(0);
  }
  else {
    // compute the depth of each subtree
    int lDepth = maxDepth(node->left);
    int rDepth = maxDepth(node->right);

    // use the larger one
    if (lDepth > rDepth) return(lDepth+1);
    else return(rDepth+1);
  }
} 

int minDepth(struct node* node) {
  if (node==NULL) {
    return(0);
  }
  else {
    // compute the depth of each subtree
    int lDepth = minDepth(node->left);
    int rDepth = minDepth(node->right);

    // use the larger one
    if (lDepth < rDepth) return(lDepth+1);
    else return(rDepth+1);
  }
} 
