#include <stdio.h>
#include <limits.h>
#include <stdlib.h>
#include <string.h>

#define N 10
#define VOLUMEN 123

int utilidad[N];
int volumen[N];
int elegido[VOLUMEN+1];
int opt[VOLUMEN+1];


int optimo(int vol) {
  
  int mejor=0;
  int i;

  elegido[vol]=-1; //////
  for (i=0; i<N; i++) {
    if (volumen[i]>vol) continue;
    int volumen_restante=vol-volumen[i];
    int p=utilidad[i]+optimo(volumen_restante);
    if (mejor<p) {
      mejor=p;
      elegido[vol]=i; //////
    }
  }
  return mejor;
}


int optimopd(int volumen_total) {
  
  int mejor=0;
  int i;

  elegido[volumen_total]=-1;
  opt[0]=0;
  int vol;

  for (vol=1;vol<=volumen_total;++vol) { 
    mejor=0;
    for (i=0; i<N; i++) {
      if (volumen[i]>vol) continue;
      int volumen_restante=vol-volumen[i];
      int p=utilidad[i]+opt[volumen_restante];
      if (mejor<p) {
	mejor=p;
	elegido[vol]=i;
      }
    }
    opt[vol]=mejor;
  }
  return opt[volumen_total];
}

int uniform(int min, int max) {
  return ((double)rand()/(RAND_MAX+1.0)) * (max-min+1) + min;
}


int main(int argc, char *argv[]) {
  int i;

  if (argc!=2) {
    printf("Uso: %s <rec o dyn>.",argv[0]);
    return 1;
  }


  
  for (i=0;i<N;i++) {
    volumen[i]=uniform(5,VOLUMEN/10);
    utilidad[i]=uniform(1,100);
    elegido[i]=-1;

    printf("producto %d: utilidad=%d vol=%d\n",i,utilidad[i],volumen[i]);
  }

  
  int opt;
  if (strcmp("rec",argv[1])==0)
    opt=optimo(VOLUMEN);
  else
    opt=optimopd(VOLUMEN);

  int volumen_restante=VOLUMEN;
  
  printf("optimo=%d\nitems:\n",opt);
  
  //  while (volumen_restante>=0 && elegido[volumen_restante]==-1) 
  // volumen_restante--;

  while (volumen_restante>0) {
    int item_elegido=elegido[volumen_restante];
    if (item_elegido==-1) break;
    printf("%d ",item_elegido);
    volumen_restante-=volumen[item_elegido];
  }
  printf("\n");
  return 0;
}


