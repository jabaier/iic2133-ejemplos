#include <stdio.h>
#include <limits.h>
#include <assert.h>
#include "config.h"
#include "utils.h"


int L[NUMBER+1];
int R[NUMBER+1];

void merge(int A[],int left, int mid, int right) {
  int i,j,k;

  for (i=left,j=0;i<=mid;i++,j++)
    L[j]=A[i];
  L[j]=INT_MAX;
  
  for (i=mid+1,j=0;i<=right;i++,j++)
    R[j]=A[i];
  R[j]=INT_MAX;
  
  i=j=0;
  
  for (k=left; k<=right; k++)
    if (L[i]<=R[j])  
      A[k]=L[i++];
    else
      A[k]=R[j++];
 }

void msort(int A[],int left, int right) {
  if (left<right) {
    int mid=(left+right)/2;
    msort(A,left,mid);
    msort(A,mid+1,right);
    merge(A,left,mid,right);
  }
}

void mergesort(int A[], int n) {
  msort(A,0,n-1);
}

int A[NUMBER];

int main() {
  int k=0;

  assert(NUMBER<INT_MAX);

  k=read_array(A);

  mergesort(A,k);

  print_array(A,k);

  return 0;
}
