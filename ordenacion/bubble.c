#include <stdio.h>
#include <assert.h>
#include <limits.h>
#include "config.h"
#include "utils.h"
#include "limits.h"

void bubble(int A[], int n) {
  int i;
  int j;
  int changed=1;
  for (i=0; i<n-1 && changed==1 ; i++) {
    changed=0;
    for (j=n-1; j>i; j--)
      if (A[j-1]>A[j]) {
	array_swap(A,j,j-1);
	changed=1;
      }
  }
}

int A[NUMBER];

int main() {
  int k=0;

  assert(NUMBER<INT_MAX);

  k=read_array(A);

  bubble(A,k);

  print_array(A,k);

  return 0;

}
